-- VERSION 0.0 as handed over

CREATE PROCEDURE [dbo].[Weekly_FridayReport_NZ]

	@CampaignHeaderID int,@Start_date date, @End_date date, @condition varchar(max),@SaveHistory varchar(1)
   ,@DeliveredEmails int, @OpenedEmails int, @ClickedEmails int, @UnsubscribedEmails int 

AS
BEGIN


SET LANGUAGE English

declare @Campaign_period_duration int
declare @Start_date_ly date
declare @End_date_ly date
declare @Start_date_lp date
declare @End_date_lp date

/*

declare @Start_date date = '20feb2018'
declare @End_date date = '06mar2018'
declare @CampaignHeaderID  int = '156'
declare @DeliveredEmails int = 89466
declare @OpenedEmails int = 26103
declare @ClickedEmails int = 739
declare @UnsubscribedEmails int = 82

declare @Campaign_period_duration int	= datediff(day,@Start_date, @End_date)
declare @Start_date_ly date				= dateadd(year,-1,@Start_date)
declare @End_date_ly date				= dateadd(year,-1,@End_date)
declare @Start_date_lp date				= dateadd(day, - @Campaign_period_duration,dateadd(day,-1,@Start_date))
declare @End_date_lp date				= dateadd(day,-1,@Start_date) 

*/


set @Campaign_period_duration= datediff(day,@Start_date, @End_date)

set @Start_date_ly=dateadd(year,-1,@Start_date)
set @End_date_ly=dateadd(year,-1,@End_date)

set @Start_date_lp=dateadd(day, - @Campaign_period_duration,dateadd(day,-1,@Start_date))
set @End_date_lp=dateadd(day,-1,@Start_date) 

print 'Campaign start date -> '+ datename(weekday,@Start_date) + ' ' + ltrim(@Start_date)
print 'Campaign start date -> '+ datename(weekday,@End_date) + ' ' + ltrim(@End_date)
print 'Campaign period -> '+ ltrim(@Campaign_period_duration+1)

print 'Reference Period (Last Year) from '+ datename(weekday,@Start_date_ly) + ' ' + ltrim(@Start_date_ly) + ' to ' + datename(weekday,@End_date_ly) + ' ' + ltrim(@End_date_ly)
print 'Reference Period (Previous Period) from '+ datename(weekday,@Start_date_lp) + ' ' + ltrim(@Start_date_lp) + ' to ' + datename(weekday,@End_date_lp) + ' ' + ltrim(@End_date_lp)

-- Check Products

declare @CheckProductSQL varchar(max)
set @CheckProductSQL =
	'
	select * 
	from [dbo].[NZBunnings_item]  --check items in edm exist
	where
	
	'

exec (@CheckProductSQL + @condition)

-- Check Products existed in comparrison periods

declare @CheckPeriodSQL1 varchar(max)
declare @CheckPeriodSQL2 varchar(max)

----if object_id('temp.sales_hist_nz') is not null drop table temp.sales_hist_nz

--select cast(calendar_date as date) calendar_date,customer_acc_sk, item_sk,[TOTAL_QUANTITY], cast(total_sales_exgst as float) total_sales_exgst
--into temp.sales_hist_nz
--from NZ_Production.[delta_sales_fact]
--where dateto is null

----select distinct calendar_date from NZ_Production.[delta_sales_fact] where dateto is null order by 1 -- 2018-01-01 - 2018-08-09

--insert into temp.sales_hist_nz
--select cast(calendar_date as date) calendar_date,customer_acc_sk, item_sk,[TOTAL_QUANTITY], cast(total_sales_exgst as float) total_sales_exgst
--from [NewZealand].[dm_sales_fact]
--where year(CALENDAR_DATE)<>2018

----select distinct calendar_date from [NewZealand].[dm_sales_fact] where year(CALENDAR_DATE)<>2018 order by 1 -- 2014-12-01 00:00:00.000 - 2017-12-31 00:00:00.000

--select distinct calendar_date
--from temp.sales_hist_nz
--where calendar_date>=dateadd(year,-1,getdate())
--order by 1


set @CheckPeriodSQL1 =
	'
	select BUSINESS_SKU_CODE,Primary_desc as Product,min(TRX_DT) as date_min
	from [dbo].[NZ_BunningsTrade_sales_fact] a 
	JOIN [dbo].[NZBunnings_item] b on a.ITEM_ID = b.ITEM_ID
	where
	
	'

set @CheckPeriodSQL2 =
	'
	group by BUSINESS_SKU_CODE,Primary_desc
	order by 3
	'

exec (@CheckPeriodSQL1 + @condition + @CheckPeriodSQL2)

-- Get sales

if object_id('dbo.sales') is not null drop table dbo.sales

declare @GetSalesSQL varchar(max)

/*
set @GetSalesSQL =
	'
	select a.* ,b.BUSINESS_SKU_CODE,b.PRIMARY_DESC,b.SUB_CLASS,b.CLASS,b.DEPARTMENT
	into dbo.sales			--pull all sales for promo items
	from production.delta_sales_fact a 
	JOIN production.staging_dm_item_dim b on a.ITEM_SK = b.ITEM_SK
	where a.dateto is null 
		AND 
	' 
	+ @condition
	+ ' and (a.TRX_DT BETWEEN ''' + ltrim(@Start_date_ly) + '''AND ''' + ltrim(@End_date_ly) 
	+ ''' OR  a.TRX_DT BETWEEN ''' + ltrim(@Start_date_lp) + ''' AND ''' + ltrim(@End_date) + ''')'
*/


select top 10 * from TRBunnings.[dbo].[Bunnings_item]

set @GetSalesSQL =
	'
	select a.* ,b.BUSINESS_SKU_CODE,b.PRIMARY_DESC,b.SUB_CLASS_NM,b.CLASS_NM,b.DEPARTMENT
	into dbo.sales			--pull all sales for promo items
	from [dbo].[NZ_BunningsTrade_sales_fact] a 
	JOIN [dbo].[NZBunnings_item] b on a.ITEM_ID = b.ITEM_ID
	JOIN [TRBunnings].[dbo].[NZBunnings_customer_dim] d on a.acct_id = d.acct_id
	where a.TRX_DT is not null

		AND
		' 
	+ @condition
	+ ' and (a.TRX_DT BETWEEN ''' + ltrim(@Start_date_ly) + '''AND ''' + ltrim(@End_date_ly) 
	+ ''' OR  a.TRX_DT BETWEEN ''' + ltrim(@Start_date_lp) + ''' AND ''' + ltrim(@End_date) + ''')'



print @GetSalesSQL

exec (@GetSalesSQL )

-- By product

if object_id('dbo.product') is not null drop table dbo.product

select BUSINESS_SKU_CODE
		,Primary_desc as Product
		,     sum(case when TRX_DT between @Start_date    and @End_date    then SOLD_QTY    else 0 end)				   as [QTY Purchased (Current)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Current)]
		,	  sum(case when TRX_DT between @Start_date_ly and @End_date_ly then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Year)]
		,CAST(sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Last Year)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
		    - sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Year $)]

		,case when    sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
			else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) 
				    / sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
		 end as [Total Growth (Last Year %)]
			
		,     sum(case when TRX_DT between @Start_date_lp and @End_date_lp then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Period)]
		,CAST(sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Sales (Last Period)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
	        - sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Period $)]

		,case when    sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
			else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
					/ sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
		 end as [Total Growth (Last Period %)]

into dbo.product
from dbo.sales
group by business_SKU_Code, PRIMARY_DESC
order by 5 desc



print('select BUSINESS_SKU_CODE
		,Primary_desc as Product
		,     sum(case when TRX_DT between @Start_date    and @End_date    then SOLD_QTY    else 0 end)				   as [QTY Purchased (Current)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Current)]
		,	  sum(case when TRX_DT between @Start_date_ly and @End_date_ly then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Year)]
		,CAST(sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Last Year)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
		    - sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Year $)]

		,case when    sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
			else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) 
				    / sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
		 end as [Total Growth (Last Year %)]
			
		,     sum(case when TRX_DT between @Start_date_lp and @End_date_lp then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Period)]
		,CAST(sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Sales (Last Period)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
	        - sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Period $)]

		,case when    sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
			else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
					/ sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
		 end as [Total Growth (Last Period %)]

into dbo.product
from dbo.sales
group by business_SKU_Code, PRIMARY_DESC
order by 5 desc')

select * from dbo.product order by 4 desc

-- Total

if object_id('dbo.total') is not null drop table dbo.total

select sum(case when TRX_DT between @Start_date    and @End_date    then SOLD_QTY    else 0 end)				    as [QTY Purchased (Current)]
 ,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Current)]
 ,     sum(case when TRX_DT between @Start_date_ly and @End_date_ly then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Year)]
 ,CAST(sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Last Year)]
 ,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
     - sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Year $)]

 ,case when    sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
 	 else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) 
	 	     / sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
  end as [Total Growth (Last Year %)]
	
 ,     sum(case when TRX_DT between @Start_date_lp and @End_date_lp then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Period)]
 ,CAST(sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Sales (Last Period)]
 ,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
     - sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Period $)]

,case when    sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
	else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
			/ sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
 end as [Total Growth (Last Period %)]

into dbo.total

from dbo.sales
order by 2 desc

select * from dbo.total

--updated 6/11/18 based on new NZ report requirements
select 'Total'
		,'Total'
		,[QTY Purchased (Current)]
		,[Total Sales (Current)]
		--,[QTY Purchased (Current)]-[QTY Purchased (Last Year)] as [Number of Units Sold Growth #]
		,[Total Growth (Last Year $)]
		,[Total Growth (Last Year %)]
		,[QTY Purchased (Last Period)]
		,[Total Growth (Last Period $)]
		,[Total Growth (Last Period %)]
		from dbo.total
union 
select 	BUSINESS_SKU_CODE
		,Product	
		,[QTY Purchased (Current)]
		,[Total Sales (Current)]
		--,[QTY Purchased (Current)]-[QTY Purchased (Last Year)] as [Number of Units Sold Growth #]
		,[Total Growth (Last Year $)]
		,[Total Growth (Last Year %)]
		,[QTY Purchased (Last Period)]
		,[Total Growth (Last Period $)]
		,[Total Growth (Last Period %)]
		from dbo.product
order by [Total Sales (Current)] desc

/*
select 'Total'
		,'Total'
		,[QTY Purchased (Current)]
		,[Total Sales (Current)]
		,[QTY Purchased (Current)]-[QTY Purchased (Last Period)] as [Number of Units Sold Growth #]
		,[Total Growth (Last Period $)]
		,[Total Growth (Last Period %)]
		from temp.total
union 
select 	BUSINESS_SKU_CODE
		,Product	
		,[QTY Purchased (Current)]
		,[Total Sales (Current)]
		,[QTY Purchased (Current)]-[QTY Purchased (Last Period)] as [Number of Units Sold Growth #]
		,[Total Growth (Last Period $)]
		,[Total Growth (Last Period %)]
		from temp.product
order by [Total Sales (Current)] desc
*/

select * from dbo.sales order by TRX_DT

while @SaveHistory = 'Y'

Begin

delete from Campaign.WeeklyReportingHistory
where campaignheaderid=@campaignheaderid

insert into Campaign.WeeklyReportingHistory
select @campaignheaderid as CampaignHeaderID, 'Product' as Report, null, null, null, null, null, null, null,*, getdate() as LastUpdated from temp.product

insert into Campaign.WeeklyReportingHistory
select @campaignheaderid as CampaignHeaderID, 'Total' as Report
			, @DeliveredEmails
			, @OpenedEmails
			, Cast(@OpenedEmails as decimal(10,2))/@DeliveredEmails
			, @ClickedEmails
			, Cast(@ClickedEmails as decimal(10,2))/@DeliveredEmails
			, @UnsubscribedEmails
			, Cast(@UnsubscribedEmails as decimal(10,2))/@DeliveredEmails
			,'n/a' as Business_SKU_CODE
			,'n/a' as Product
			, *
			, getdate() as LastUpdated 
from dbo.total


set  @SaveHistory = 'N'

END





End


