# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 15:12:17 2020

@author: vytautas b

This scirpt helps identifying the structure of and objects within a .pptx template
(or any presentation, really)
"""

from pathlib import Path

from pptx import Presentation


def analyze_presentation(presentation):
    """ Function analyzes presenatation listing all objects on each slide.
    """
    for sl_number, slide in enumerate(presentation.slides):
        print("Slide number {} contains:".format(sl_number))
        for shape in slide.shapes:
            print(shape.name, shape.shape_type)
        print("\n ")    


def analyze_table(table):
    for ri, row in enumerate(table.rows):
        for ci, col in enumerate(table.columns):
            print('Cell ({}{}) contains: {}'.format(ri, ci, table.cell(ri, ci).text))


def analyze_cell(cell):
    for pi, paragraph in enumerate(cell.text_frame.paragraphs):
        print("Paragraph {} reads: {}".format(pi, paragraph.text))    


HOME = Path('C:/Users/vytau/Documents/Bunnings/test_1')
PRESENTATION_TO_ANALYZE = 'Bunnings_Standard_Campaign_New_Template.pptx'
prs = Presentation(HOME / PRESENTATION_TO_ANALYZE)

# Start by analysis of the template slide-by-slide
analyze_presentation(prs)

# Go into further details by specifying the following (whichever applicable):
#table = prs.slides[2].shapes[2].table
#analyze_table(table)

# Analyze finer and finer details such as parameters/content of a cell from a table:
#cell = table.cell(5, 1)
#analyze_cell(cell)