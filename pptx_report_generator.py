# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 12:26:17 2019

@author: vytautas b

This script generates Bunnings report (PowerPoint presentation)
It needs the report .pptx template in HOME directory, campaign visuals and 
Taguchi reports (.csv) as separate files placed in "Creative" and "Import_Data"
sub-folders, respectively. Image names must include Taguchi "activity_id"
because the "campaign_code" appeared to not always be unique (identical for 
both islands sometimes, e.g.).
Sales data are queried directly from the database, the credentials
for which must be specified in the config file "bunnings.conf".
"""

from datetime import datetime, timedelta
from pathlib import Path
import configparser
import glob
import locale
import csv

import pyodbc
import pandas as pd
from dateutil.relativedelta import relativedelta
from pptx import Presentation
from pptx.util import Cm, Pt
from pptx.enum.text import PP_PARAGRAPH_ALIGNMENT, MSO_ANCHOR
from pptx.dml.color import RGBColor


cfg = configparser.ConfigParser()
cfg.read('bunnings.conf')

locale.setlocale(locale.LC_ALL, 'en_nz')


def connect_to_db(driver, server, port, db, usr, pwd):
    conn_str = f'DRIVER={driver};SERVER={server};PORT={port};DATABASE={db};UID={usr};PWD={pwd}'

    try:
        print(f"-- Connecting to database [{db}]...")
        conn = pyodbc.connect(conn_str)
        print("-- Connection established")
    except pyodbc.OperationalError as ex:
        sqlstate = ex.args[0]
        if sqlstate == '08001':
            print("Check connection to VPN")   

    return conn    


def get_sales_data(sku_codes, start_date_dt, end_date_dt):
    """
    Function obtainns relevant sales data for the provided list of SKU codes.
    Essentially, it runs the stored procedure and pics the required outputs.

    Parameters
    ----------
    sku_codes : list
        List of SKU codes of the items to be reported.

    Returns
    df : DataFrame
        Sales data
    top_segment : list
        Top four buyer segments
    """
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', -1)

    driver = cfg['database']['driver']
    server = cfg['database']['server']
    port = cfg['database']['port']
    db = cfg['database']['db']
    usr = cfg['database']['usr']
    pwd = cfg['database']['pwd']
    
    sku_double_quoted_list = ','.join("''" + x + "''" for x in sku_codes)
    from_date = start_date_dt.strftime('%d%b%Y').lower() # eg "28nov2019"
    to_date = end_date_dt.strftime('%d%b%Y').lower()
    
    # Extract sales data:
    sql_statement = f"""
    exec {db}.[dbo].[{STORED_PROCEDURE}] '999',
    '{from_date}','{to_date}'
    ,'BUSINESS_SKU_CODE in ( {sku_double_quoted_list} )'
    ,'N',null,null,null,null ;
    """
    
    conn = connect_to_db(driver, server, port, db, usr, pwd)
    
    if conn:
        cursor = conn.cursor()
        
        print(f"-- Performing query")
        out_frames = multi_query_to_df(sql_statement, cursor)
        print("-- Query successful")
    
        sales_df = out_frames[3] # "total sales" output from the stored procedure
        print("-- Sales data retrieved: ")
        print(sales_df)
        
        top_df = out_frames[4] #   "top segment" output from the stored procedure
        top_results = top_df.iloc[0:4, 1]
        top_segments = [i.capitalize() for i  in top_results]
        #print("-- Top segments retrieved: ")
        #print(top_df)
        
        cursor.close()
        conn.close()
    
    return sales_df, top_segments


def multi_query_to_df(exec_statement, cursor):
    """
    Function executes a query that generates multiple sets of results and puts
    the results into dataframes. Dataframes are stored within a list.

    Parameters
    ----------
    exec_statement : str
        Statement to be executed.
    cursor : pyodbc.Cursor
        Cursor for the execution.

    Returns
    out_df_list : list
        List of dataframes, each containing one of the multpile query outputs.
    """

    data_frames = []
    no_count = """ SET NOCOUNT ON; """ # exclude empty resultsets
    
    cursor.execute(no_count + exec_statement)
    result = cursor.fetchall()
    while result:
        col_names = [x[0] for x in cursor.description]
        data = [tuple(x) for x in result]  # pyodbc.Row objects to tuples
        data_frames.append(pd.DataFrame(data, columns=col_names))
        if cursor.nextset():
            result = cursor.fetchall()
        else:
            result = None
            
    return data_frames


def delete_paragraph(paragraph):
    p = paragraph._p
    parent_element = p.getparent()
    parent_element.remove(p)
    

def create_sales_table(on_slide):
    data_table = on_slide.shapes.add_table(rows = rows+1, cols = cols,
                                            left = Cm(7.43),
                                            top = Cm(10.86),
                                            width = Cm(17.4),
                                            height = Cm(5.65)).table
    data_table.rows[0].height = Cm(0.91)
    for row in data_table.rows:
        row.height = Cm(0.32)
    data_table.columns[0].width = Cm(1.05)
    data_table.columns[1].width = Cm(5.41)
    data_table.columns[2].width = Cm(0.95) 
    data_table.columns[3].width = Cm(1.97)
    data_table.columns[4].width = Cm(1.58)
    data_table.columns[5].width = Cm(1.44)
    data_table.columns[6].width = Cm(1.24)
    data_table.columns[7].width = Cm(1.82)
    data_table.columns[8].width = Cm(1.54)
    
    for cell in data_table.iter_cells():
        cell.margin_left = Cm(0.02)
        cell.margin_right = Cm(0.1)
        cell.margin_top = Cm(0.02)
        cell.margin_bottom = Cm(0.0)
        p = cell.text_frame.paragraphs[0]
        p.alignment = PP_PARAGRAPH_ALIGNMENT.RIGHT
        p.font.name= "Calibri"
        p.font.size = Pt(7)
    
    for idx, col_name in enumerate(col_names):
        p = data_table.cell(0, idx).text_frame.paragraphs[0]
        p.text = col_name
        p.font.size = Pt(7)
        p.font.color.rgb = RGBColor(0, 176, 240)
        p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER
        data_table.cell(0, idx).vertical_anchor = MSO_ANCHOR.MIDDLE
        
    for row in range(rows):
        for col in [0, 1]:
            p = data_table.cell(row+1, col).text_frame.paragraphs[0]
            p.alignment = PP_PARAGRAPH_ALIGNMENT.LEFT
            p.text = str(data[row, col])
    
    for row in range(rows):
        for col in [2, 6]:
            p = data_table.cell(row+1, col).text_frame.paragraphs[0]
            p.text = f"{data[row, col]:n}"
    
    for row in range(rows):
        p = data_table.cell(row+1, 3).text_frame.paragraphs[0]
        p.text = f"${data[row, 3]:,}"
    
    for row in range(rows):
        for col in [4,7]:
            p = data_table.cell(row+1, col).text_frame.paragraphs[0]
            if data[row, col] >= 0:
                txt = f"${data[row, col]:,}"
            else:
                txt = f"-${abs(data[row, col]):,}"
            p.text = txt
    
    for row in range(rows):
        p = data_table.cell(row+1, 5).text_frame.paragraphs[0]
        if data[row, 3] != data[row, 4]:
            txt = f"{data[row, 5]*100:n}%"
        else:
            txt = "-"
        p.text = txt
            
    for row in range(rows):
        p = data_table.cell(row+1, 8).text_frame.paragraphs[0]
        if data[row, 3] != data[row, 7]:
            txt = f"{data[row, 8]*100:n}%"
        else:
            txt = "-"
        p.text = txt
            
    for row in range(rows):
        for col in range(cols):
            p = data_table.cell(row+1, col).text_frame.paragraphs[0]
            p.font.color.rgb = RGBColor(0, 0, 0)


############################################
###
### input variables (none should be present outside this block)
###
############################################
HOME = Path(cfg['configuration']['HOME']) # holds the template
STORED_PROCEDURE = cfg['configuration']['STORED_PROCEDURE'] # change for testing only
TEMPLATE_NAME = cfg['configuration']['TEMPLATE_NAME']

WORK = cfg['report']['WORK'] # directory for current reports relative to HOME

WEEK_CODE = cfg['report']['WEEK_CODE']
CAMPAIGN_CODE = cfg['report']['CAMPAIGN_CODE']
ACTIVITY_ID = cfg['report']['ACTIVITY_ID']
CAMPAIGN_NAME = cfg['report']['CAMPAIGN_NAME']
CAMPAIGN_NAME_SHORT = cfg['report']['CAMPAIGN_NAME_SHORT'] # used for presentation name
START_DATE = cfg['report']['START_DATE']
END_DATE = cfg['report']['END_DATE']

SKU_CODES = [code.strip() for code in cfg['report']['SKU_CODES'].split(sep=",") if code]

############################################
###
### pre-processing
###
############################################
prs = Presentation(HOME / TEMPLATE_NAME)


#----------- setting dates -----------
start_date_dt = datetime.strptime(START_DATE, '%Y-%m-%d')
if END_DATE:
    end_date_dt = datetime.strptime(END_DATE, '%Y-%m-%d')
else:
    print("-- No end date provided, using reporting week instead")
    end_date_dt = datetime.strptime(WEEK_CODE, 'M %d %b %Y') 
duration = end_date_dt - start_date_dt
start_date_lp = start_date_dt - duration - timedelta(days = 1)
start_date_ly = start_date_dt - relativedelta(years=1)
end_date_lp = start_date_dt - timedelta(days = 1)
end_date_ly = end_date_dt - relativedelta(years=1)
campaign_date_range = f"{start_date_dt.strftime('%d %b')} \u2013 {end_date_dt.strftime('%d %b')}"


#----------- path(s) to image(s) -----------
images = [Path(image) for image in glob.glob(str(HOME / WORK) + f'/Creative/*{ACTIVITY_ID}*')]
for image in images:
    print(f"-- Found image(s): {image}")

#----------- Taguchi data -----------
stats_csv_loc = Path(glob.glob(str(HOME / WORK) + f'/Import_Data/*{ACTIVITY_ID}*')[0])
with open(stats_csv_loc) as STATS_CSV:
    print(f"-- Parsing Taguchi file: {stats_csv_loc}")
    stats_csv = csv.reader(STATS_CSV, delimiter=',')
    next(stats_csv) # skip headder row
    data_row = next(stats_csv)
    OPENED = int(data_row[7])
    CLICKED = int(data_row[18])
    OPEN_RATE = data_row[19]
    CLICK_THROUGH_RATE = data_row[21]

 
#----------- sales data -----------
if SKU_CODES:
    df, TOP_SEGMENTS = get_sales_data(SKU_CODES, start_date_dt, end_date_dt)

    rows, cols = df.shape
    col_names = list(df.columns)
    data = df.values
    
    SALES = data[0, 3]
    YOY_GROWTH = data[0, 5] * 100
    MOM_GROWTH = data[0, 8] * 100
    MOST_POPULAR = data[1, 1]
else:
    print("-- No SKU codes provided, continue reporting Taguchi results only")

############################################
###
### process slides
###
############################################
#----------- slide 1 -----------
current_slide = prs.slides[0]
text_box = current_slide.shapes[1]
text_box.text = WEEK_CODE

#----------- slide 2 -----------
current_slide = prs.slides[1]
text_box = current_slide.shapes[0]
text_box.text_frame.paragraphs[0].text = CAMPAIGN_CODE
text_box.text_frame.paragraphs[1].text = CAMPAIGN_NAME
text_box = current_slide.shapes[1]
text_box.text = f"({campaign_date_range})"


#----------- slide 3 (main info) -----------
current_slide = prs.slides[2]
table = current_slide.shapes[0].table

p = table.cell(0, 0).text_frame.paragraphs[0]
p.text = CAMPAIGN_CODE + " " + CAMPAIGN_NAME
p.font.size = Pt(14)

p = table.cell(0, 5).text_frame.paragraphs[0]
p.text = campaign_date_range
p.font.size = Pt(14)

p = table.cell(1, 2).text_frame.paragraphs[0]
p.text = f'{OPENED:,}'
p.font.size = Pt(9)

table.cell(3, 1).vertical_anchor = MSO_ANCHOR.MIDDLE
p = table.cell(3, 1).text_frame.paragraphs[0]
p.text = CAMPAIGN_NAME_SHORT
p.font.size = Pt(8)

p = table.cell(3, 2).text_frame.paragraphs[0]
p.text = OPEN_RATE
p.font.size = Pt(9)
p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER

p = table.cell(3, 3).text_frame.paragraphs[0]
p.text = CLICK_THROUGH_RATE
p.font.size = Pt(9)
p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER

p = table.cell(5, 1).text_frame.paragraphs
p[1].text = f"This campaign was delivered to {OPENED:,} active Bunnings customers with over {CLICKED:,} opening the eDM"
p[1].font.size = Pt(9)

if SKU_CODES:
    phrase_one = f"Overall total sales in the featured products over the campaign period was ${SALES:,}. "
    sub_phr_a = "This equates to "
    sub_phr_b = "compared to the pre campaign and there "
    sub_phr_c = "compared to same time last year.*"

    if df.iat[0, 3] == df.iat[0, 7]: # compare Sales and Sales Growth MoM
        phrase_two = "There were no sales during the pre-campaign period and there "
    else:
        if MOM_GROWTH > 0:
            phrase_two = sub_phr_a +f"an increase in sales of {MOM_GROWTH}% " + sub_phr_b
        elif MOM_GROWTH < 0:
            phrase_two = sub_phr_a + f"a decrease in sales of {abs(MOM_GROWTH)}% " + sub_phr_b
        else:
            phrase_two = sub_phr_a + "no increase in sales " + sub_phr_b

    if df.iat[0, 3] == df.iat[0, 4]: # compare Sales and Sales Growth YoY
        phrase_three = "were no sales the same time last year.*"
    else:
        if YOY_GROWTH > 0:
            phrase_three = f"was an increase in sales of {YOY_GROWTH}% " + sub_phr_c
        elif YOY_GROWTH < 0:
            phrase_three = f"was a decrease in sales of {abs(YOY_GROWTH)}% " + sub_phr_c
        else: # the Sales Growth % truly zero
            phrase_three = "was no increase in sales " + sub_phr_c
        
    p[2].text = phrase_one + phrase_two + phrase_three
    p[2].font.size = Pt(9)
    
    p[3].text = f"The most popular product during the campaign was {MOST_POPULAR}"
    p[3].font.size = Pt(9)

    p[4].text = "Top spending segments purchasing the targeted products are: "
    p[4].font.size = Pt(9)   

    p[5].text = TOP_SEGMENTS[0]
    p[5].font.size = Pt(9)
    p[6].text = TOP_SEGMENTS[1]
    p[6].font.size = Pt(9)
    p[7].text = TOP_SEGMENTS[2]
    p[7].font.size = Pt(9)
    p[8].text = TOP_SEGMENTS[3]
    p[8].font.size = Pt(9)
else:
    delete_paragraph(p[2])
    delete_paragraph(p[3])
    delete_paragraph(p[4])
    delete_paragraph(p[5])
    delete_paragraph(p[6])
    delete_paragraph(p[7])
    delete_paragraph(p[8])

#----------- date range specifier -----------
text_box = current_slide.shapes[1]
if SKU_CODES:
    text_box.text_frame.paragraphs[1].text = f"Same period last year: {start_date_ly.strftime('%Y-%m-%d')} to {end_date_ly.strftime('%Y-%m-%d')}"
    text_box.text_frame.paragraphs[1].font.size = Pt(7.5)
    text_box.text_frame.paragraphs[2].text = f"Pre-campaign period: {start_date_lp.strftime('%Y-%m-%d')} to {end_date_lp.strftime('%Y-%m-%d')}"
    text_box.text_frame.paragraphs[2].font.size = Pt(7.5)


#----------- sales data table -----------
if SKU_CODES:
    create_sales_table(current_slide)
    

#----------- insert/format artwork -----------
for id, image in enumerate(images):
    width = 6.42 / len(images)
    left = 0.98 + width * id
    top = 3.8
    if len(images) < 3:
        height = 11.8
    else:
        height = 6 # not to stretch out too much
    path = str(image)
    current_slide.shapes.add_picture(path, Cm(left), Cm(top), Cm(width), Cm(height))


OUT_NAME = CAMPAIGN_CODE + " " + CAMPAIGN_NAME_SHORT + ".pptx"
prs.save(HOME / WORK / OUT_NAME)
print(f"--- Report {OUT_NAME} generated successfully ---")