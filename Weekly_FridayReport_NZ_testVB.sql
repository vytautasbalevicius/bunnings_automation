CREATE PROCEDURE [dbo].[Weekly_FridayReport_NZ_testVB]

	@CampaignHeaderID int,@Start_date date, @End_date date, @condition varchar(max),@SaveHistory varchar(1)
   ,@DeliveredEmails int, @OpenedEmails int, @ClickedEmails int, @UnsubscribedEmails int 

AS
BEGIN
-----------------------------
-- This procedure is used to generate 5 sets of results:
--
-- OUTPUT 1: checks if the listed items exist
-- OUTPUT 2: checks if the listed items existed in comparrison periods
-- OUTPUT 3: shows results of aggregation by listed items
-- OUTPUT 4: shows final report
-- OUTPUT 5: shows top spending segments for the listed itmens during campaign period
--
-- Procedure has capacity to store campaign sales and Taguchi data into database (@SaveHistory flag)
-----------------------------
	
SET LANGUAGE English

declare @Campaign_period_duration int
declare @Start_date_ly date
declare @End_date_ly date
declare @Start_date_lp date
declare @End_date_lp date


set @Campaign_period_duration= datediff(day,@Start_date, @End_date)

set @Start_date_ly=dateadd(year,-1,@Start_date)
set @End_date_ly=dateadd(year,-1,@End_date)

set @Start_date_lp=dateadd(day, - @Campaign_period_duration,dateadd(day,-1,@Start_date))
set @End_date_lp=dateadd(day,-1,@Start_date) 

print 'Campaign start date -> '+ datename(weekday,@Start_date) + ' ' + ltrim(@Start_date)
print 'Campaign start date -> '+ datename(weekday,@End_date) + ' ' + ltrim(@End_date)
print 'Campaign period -> '+ ltrim(@Campaign_period_duration+1)

print 'Reference Period (Last Year) from '+ datename(weekday,@Start_date_ly) + ' ' + ltrim(@Start_date_ly) + ' to ' + datename(weekday,@End_date_ly) + ' ' + ltrim(@End_date_ly)
print 'Reference Period (Previous Period) from '+ datename(weekday,@Start_date_lp) + ' ' + ltrim(@Start_date_lp) + ' to ' + datename(weekday,@End_date_lp) + ' ' + ltrim(@End_date_lp)





-- Check Products: OUTPUT 1
--
declare @CheckProductSQL varchar(max)

set @CheckProductSQL =
	'
	select * 
	from [dbo].[NZBunnings_item]  --check items in edm exist
	where
	'
    + @condition
exec (@CheckProductSQL)





-- Check if products existed in comparrison periods: OUTPUT 2
--
declare @CheckPeriodSQL varchar(max)

set @CheckPeriodSQL =
	'
	select BUSINESS_SKU_CODE, Primary_desc as Product, min(TRX_DT) as date_min
	from [dbo].[NZBunnings_item] a 
	LEFT JOIN [dbo].[NZ_BunningsTrade_sales_fact] b on a.ITEM_ID = b.ITEM_ID
	where
	'
    + @condition +
	'
	group by BUSINESS_SKU_CODE,Primary_desc
	order by 3
	'
exec (@CheckPeriodSQL)





-- Get sales for specific items and periods
if object_id('dbo.sales') is not null drop table dbo.sales

declare @GetSalesSQL varchar(max)

set @GetSalesSQL =
	'
	with a as(
		select SOLD_QTY, TOT_EXCL_GST_AMT, TRX_DT, ACCT_ID, ITEM_ID
		from [dbo].[NZ_BunningsTrade_sales_fact]
		where TRX_DT is not null
		and (TRX_DT BETWEEN ''' + ltrim(@Start_date_ly) + ''' AND ''' + ltrim(@End_date_ly) 
		+ ''' OR TRX_DT BETWEEN ''' + ltrim(@Start_date_lp) + ''' AND ''' + ltrim(@End_date) + ''')
	)
	select COALESCE(a.SOLD_QTY, 0) as SOLD_QTY
		,COALESCE(a.TOT_EXCL_GST_AMT, 0) as TOT_EXCL_GST_AMT
		,a.TRX_DT
		,a.ACCT_ID
		,b.BUSINESS_SKU_CODE
		,b.PRIMARY_DESC
	into dbo.sales			-- pull all sales for promo items
	from [dbo].[NZBunnings_item] b
	LEFT JOIN a on a.ITEM_ID = b.ITEM_ID
	where
    '
	+ @condition

print @GetSalesSQL

exec (@GetSalesSQL)





-- Aggregation by product
if object_id('dbo.product') is not null drop table dbo.product

select BUSINESS_SKU_CODE
		,Primary_desc as Product
		,     sum(case when TRX_DT between @Start_date    and @End_date    then SOLD_QTY    else 0 end)				   as [QTY Purchased (Current)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Current)]
		,	  sum(case when TRX_DT between @Start_date_ly and @End_date_ly then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Year)]
		,CAST(sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Last Year)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
		    - sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Year $)]

		,case when    sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
			else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) 
				    / sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
		 end as [Total Growth (Last Year %)]
			
		,     sum(case when TRX_DT between @Start_date_lp and @End_date_lp then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Period)]
		,CAST(sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Sales (Last Period)]
		,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
	        - sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Period $)]

		,case when    sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
			else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
					/ sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
		 end as [Total Growth (Last Period %)]

into dbo.product
from dbo.sales
group by BUSINESS_SKU_CODE, PRIMARY_DESC
order by 5 desc





-- Show results of aggregation: OUTPUT 3
--
select * from dbo.product order by 4 desc





-- Total
if object_id('dbo.total') is not null drop table dbo.total

select sum(case when TRX_DT between @Start_date    and @End_date    then SOLD_QTY    else 0 end)				    as [QTY Purchased (Current)]
 ,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Current)]
 ,     sum(case when TRX_DT between @Start_date_ly and @End_date_ly then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Year)]
 ,CAST(sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) as decimal(10,2)) as [Total Sales (Last Year)]
 ,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
     - sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Year $)]

 ,case when    sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
 	 else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end) 
	 	     / sum(case when TRX_DT between @Start_date_ly and @End_date_ly then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
  end as [Total Growth (Last Year %)]
	
 ,     sum(case when TRX_DT between @Start_date_lp and @End_date_lp then SOLD_QTY    else 0 end)                   as [QTY Purchased (Last Period)]
 ,CAST(sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Sales (Last Period)]
 ,CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
     - sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) as [Total Growth (Last Period $)]

,case when    sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) = 0 then 0 
	else CAST(sum(case when TRX_DT between @Start_date    and @End_date    then TOT_EXCL_GST_AMT else 0 end)
			/ sum(case when TRX_DT between @Start_date_lp and @End_date_lp then TOT_EXCL_GST_AMT else 0 end) AS decimal(10,2)) - 1 
 end as [Total Growth (Last Period %)]

into dbo.total
from dbo.sales
order by 2 desc





-- Final report: OUTPUT 4
--
select 'Total' as [SKU]
		,'Total' as [Product]
		,[QTY Purchased (Current)] as [Units Sold (#)]
		,[Total Sales (Current)] as [Sales ($)]
		,[Total Growth (Last Year $)] as [Sales Growth YoY ($)]
		,[Total Growth (Last Year %)] as [Sales Growth YoY (%)]
		,[QTY Purchased (Last Period)] as [Units Sold Prior Month (#)]
		,[Total Growth (Last Period $)] as [Sales Growth MoM ($)]
		,[Total Growth (Last Period %)] as [Sales Growth MoM (%)]
		from dbo.total
union 
select 	BUSINESS_SKU_CODE as [SKU]
		,Product as [Product]
		,[QTY Purchased (Current)] as [Units Sold (#)]
		,[Total Sales (Current)]   as [Sales ($)]
		,[Total Growth (Last Year $)] as [Sales Growth YoY ($)]
		,[Total Growth (Last Year %)] as [Sales Growth YoY (%)]
		,[QTY Purchased (Last Period)] as [Units Sold Prior Month (#)]
		,[Total Growth (Last Period $)] as [Sales Growth MoM ($)]
		,[Total Growth (Last Period %)] as [Sales Growth MoM (%)]
		from dbo.product
order by [Total Sales (Current)] desc





-- Top spending segments: OUTPUT 5
--
select PRIMARY_SEGMENT, SECONDARY_SEGMENT_V2, count(distinct ACCT_NUM), sum(TOT_EXCL_GST_AMT)
from dbo.sales a join [TRBunnings].[dbo].[NZBunnings_customer_dim] b on a.ACCT_ID=b.ACCT_ID
where TRX_DT >= @Start_date
group by PRIMARY_SEGMENT, SECONDARY_SEGMENT_V2
order by 4 desc ;





while @SaveHistory = 'Y'

Begin

delete from Campaign.WeeklyReportingHistory
where campaignheaderid=@campaignheaderid

insert into Campaign.WeeklyReportingHistory
select @campaignheaderid as CampaignHeaderID, 'Product' as Report, null, null, null, null, null, null, null,*, getdate() as LastUpdated from temp.product

insert into Campaign.WeeklyReportingHistory
select @campaignheaderid as CampaignHeaderID, 'Total' as Report
			, @DeliveredEmails
			, @OpenedEmails
			, Cast(@OpenedEmails as decimal(10,2))/@DeliveredEmails
			, @ClickedEmails
			, Cast(@ClickedEmails as decimal(10,2))/@DeliveredEmails
			, @UnsubscribedEmails
			, Cast(@UnsubscribedEmails as decimal(10,2))/@DeliveredEmails
			,'n/a' as Business_SKU_CODE
			,'n/a' as Product
			, *
			, getdate() as LastUpdated 
from dbo.total

set  @SaveHistory = 'N'

End



END

