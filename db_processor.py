# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 11:15:08 2019

@author: vytautas b
"""
from datetime import datetime

import pyodbc
import pandas as pd

import config_bunnings as cfg

# to-be-moved to, e.g., env vars
driver = cfg.driver
server = cfg.server
port = cfg.port
db = cfg.db
usr = cfg.usr
pwd = cfg.pwd


def multi_query_to_df(exec_statement,cursor):
    """
    Function executes a query that generates multiple sets of results and puts
    the results into dataframes. Dataframes are stored within a list.

    Parameters
    ----------
    exec_statement : str
        Statement to be executed.
    cursor : pyodbc.Cursor
        Cursor for the execution.

    Returns
    out_df_list : list
        List of dataframes, each containing one of the multpile query outputs.
    """

    data_frames = []
    no_count = """ SET NOCOUNT ON; """ # exclude empty resultsets
    
    cursor.execute(no_count + exec_statement)
    result = cursor.fetchall()
    while result:
        col_names = [x[0] for x in cursor.description]
        data = [tuple(x) for x in result]  # pyodbc.Row objects to tuples
        data_frames.append(pd.DataFrame(data, columns=col_names))
        if cursor.nextset():
            result = cursor.fetchall()
        else:
            result = None
            
    return data_frames


SKU_CODES = ['0104015', '0104016', '0104017', '0104018']
START_DATE = "2019-11-21"
END_DATE = "2019-12-21"

sku_quoted_list = ', '.join("'" + x + "'" for x in SKU_CODES)
sku_double_quoted_list = ', '.join("''" + x + "''" for x in SKU_CODES)

start_date_dt = datetime.strptime(START_DATE, '%Y-%m-%d')
end_date_dt = datetime.strptime(END_DATE, '%Y-%m-%d')

conn = pyodbc.connect(f'DRIVER={driver};SERVER={server};PORT={port};DATABASE={db};UID={usr};PWD={pwd}')

cursor = conn.cursor()

# # Check last transaction date:
# sql_statement = f"select max (TRX_DT) from {db}.[dbo].[NZ_BunningsTrade_sales_fact] ;"
# cursor.execute(sql_statement)
# row = cursor.fetchone()
# print(f"Last transacion record in the DB: {row[0].strftime('%Y %b %d')}")

# # Check items by their SKU codes:
# sql_statement = f"""
# select * from {db}.[dbo].[NZBunnings_item] where BUSINESS_SKU_CODE 
# in ( {sku_quoted_list}  )   ;
# """
# df = pd.read_sql(sql_statement, conn)
# print(df)

# Extract sales data:
sql_statement = f"""
exec {db}.[dbo].[Weekly_FridayReport_NZ_testVB] '999',
'{start_date_dt.strftime('%d%b%Y').lower()}','{end_date_dt.strftime('%d%b%Y').lower()}'
,'BUSINESS_SKU_CODE in (  {sku_double_quoted_list} )'
,'N',null,null,null,null   ;
"""
out_frames = multi_query_to_df(sql_statement, cursor)
# relevant Weekly_FridayReport_NZ output:
print(out_frames[3])

# Pick top four segments (requires the above SQL having been run):
sql_statement = f"""
select PRIMARY_SEGMENT, SECONDARY_SEGMENT_V2, count(distinct ACCT_NUM), sum(TOT_EXCL_GST_AMT)
from dbo.sales a join {db}.[dbo].[NZBunnings_customer_dim] b on a.ACCT_ID=b.ACCT_ID
where TRX_DT >= '{START_DATE}'
group by PRIMARY_SEGMENT, SECONDARY_SEGMENT_V2
order by 4 desc ;"""
cursor.execute(sql_statement)
top_results = cursor.fetchall()[:4]
top_segments = [i[1].capitalize() for i  in top_results]
print(top_segments)


cursor.close()
conn.close()